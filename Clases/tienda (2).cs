﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practica_2
{
    public class Tienda
    {
        private int numeroLocal;
        private string Direccion;
        private double tamanioTienda;
        private string color;
        private string tipo;



        public Tienda(int numero, string dire, double tamanio, string color1,string alimentacion)

        {
            numeroLocal = numero;
            Direccion = dire;
            tamanioTienda = tamanio;
            color = color1;
            tipo = alimentacion;
        }
        public void numero()
        {
            Console.WriteLine("El numero es: "+ numeroLocal);
        }
        public void local(string alimentacion)
        {
            Console.WriteLine("la tienda es de: " + tipo);
        }
        public override string ToString()

        {
            return "La tienda esta en el numero " + numeroLocal + ", de la calle " + Direccion + tamanioTienda + color;
        }



        public void setColor(string color2)
        {
            color = color2;
        }
        public string getcolor()
        {
            return color;
        }
         public void setDireccion(string dire1)
            {
                Direccion = dire1;
            }
            public string getDireccion()
            {
                return Direccion;
            }
            public void setnumeroLocal(int numero1)
            {
                numeroLocal = numero1;
            }
            public int getnumeroLocal()
            {
                return numeroLocal;
            }
            public void settamanioTienda(double tamanio1)
            {
                tamanioTienda = tamanio1;
            }
            public double gettamanioTienda()
            {
                return tamanioTienda;
            }
            public void settipo(string alimentacion1)
            {
                tipo = alimentacion1;
            }
            public string gettipo()
            {
                return tipo;
            }
        }
        
    }
}
