﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practica_2
{
    public class Guchi
    {
        private int numeroLocal;
        private string Direccion;
        private double tamanioTienda;
        private string color;
        private string tipo;



        public Guchi(int numero, string dire, double tamanio, string color1, string alimentacion)

        {
            if (numero != 45)
            {
                throw new Exception("No es este numero");
            }
            numeroLocal = numero;
            if (dire != "c/ Lucrecia" | dire != "c/ Suances")
            {
                throw new Exception("No está en esta calle");
            }
            Direccion = dire;
            if (tamanio < 100)
            {
                throw new Exception("No tiene esa medida");
            }
            tamanioTienda = tamanio;
            if (color1 != "gris" | color1 != "negro")
            {
                throw new Exception("No es este color de fachada");
            }
            color = color1;
            if (tipo != "alimentacion" | tipo != "ropa")
            {
                throw new Exception("No ese ese tipo de tienda");
            }
            tipo = alimentacion;
        }
        public void numero()
        {
            Console.WriteLine("El numero es: " + numeroLocal);
        }
        public void local(string alimentacion)
        {
            Console.WriteLine("la tienda es de: " + tipo);
        }
        public override string ToString()

        {
            return "La tienda esta en el numero " + numeroLocal + ", de la calle " + Direccion + ", con un tamaño de: " + tamanioTienda + ", fachada de color: " + color + "y es de tipo: " + tipo;
        }



        public void setColor(string color2)
        {
            color = color2;
        }
        public string getcolor()
        {
            return color;
        }
        public void setDireccion(string dire1)
        {
            Direccion = dire1;
        }
        public string getDireccion()
        {
            return Direccion;
        }
        public void setnumeroLocal(int numero1)
        {
            numeroLocal = numero1;
        }
        public int getnumeroLocal()
        {
            return numeroLocal;
        }
        public void settamanioTienda(double tamanio1)
        {
            tamanioTienda = tamanio1;
        }
        public double gettamanioTienda()
        {
            return tamanioTienda;
        }
        public void settipo(string alimentacion1)
        {
            tipo = alimentacion1;
        }
        public string gettipo()
        {
            return tipo;
        }
    }

}
