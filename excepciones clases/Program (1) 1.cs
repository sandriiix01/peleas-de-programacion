﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practica_2
{
    public class Controlador_Tienda
    {
        static void Main(string[] args)
        {
            try
            {
                Guchi Guchi = new Guchi(13, "c/ Lucrecia", 260, "gris", "alimentacion");
                Guchi Guxxi = new Guchi(46, "c/ Suances", 180, "negro", "cosas");
                Guxxi.setnumeroLocal(46);
                Guxxi.setDireccion("c/Suances");
                Guxxi.settamanioTienda(180);
                Guxxi.setColor("negro");
                Guxxi.settipo("cosas");
                Console.WriteLine(Guchi.ToString());
                Console.WriteLine(Guxxi.ToString());
                Guchi.local("Canada");
                Guchi.numero();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("se cierra el programa...");
            }

        }
    }

}
